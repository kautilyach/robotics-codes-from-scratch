'''
    Linear Quadratic tracker applied on a via point example

    Copyright (c) 2021 Idiap Research Institute, http://www.idiap.ch/
    Written by Julius Jankowski <julius.jankowski@idiap.ch>,
    Sylvain Calinon <https://calinon.ch>

    This file is part of RCFS.

    RCFS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3 as
    published by the Free Software Foundation.

    RCFS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RCFS. If not, see <http://www.gnu.org/licenses/>.
'''

import numpy as np
from math import factorial
import matplotlib.pyplot as plt

# Parameters
# ===============================
param = lambda: None # Lazy way to define an empty class in python
param.dt = 1e-2  # Time step length
param.nbPoints = 1 # Number of target point
param.nbDeriv = 2 # Order of the dynamical system
param.nbVarPos = 2 # Number of position variable
param.nbData = 100  # Number of datapoints
param.rfactor = 1e-9  # Control weight term

nb_var = param.nbVarPos * param.nbDeriv + 1 # Dimension of state vector
R = np.identity( (param.nbData-1) * param.nbVarPos ) * param.rfactor  # Control cost matrix
Q = np.zeros((nb_var * param.nbData, nb_var * param.nbData)) # Task precision for augmented state

via_points = []
via_point_timing = np.linspace(0, param.nbData-1, param.nbPoints+1)

for tv_ in via_point_timing[1:]:
    tv = int(tv_)
    tv_slice = slice(tv*nb_var, (tv+1)*nb_var)
    X_d_tv = np.concatenate((np.random.uniform(size=param.nbVarPos), np.zeros(param.nbVarPos)))
    via_points.append(X_d_tv[:param.nbVarPos])
    print("via point: ", X_d_tv)
    Q_tv = np.eye(nb_var-1)
    if tv < param.nbData-1:
      Q_tv[param.nbVarPos:, param.nbVarPos:] = 1e-6*np.eye(param.nbVarPos) # Don't track velocities on the way
    Q_inv = np.zeros((nb_var, nb_var))
    Q_inv[:nb_var-1, :nb_var-1] = np.linalg.inv(Q_tv) + np.outer(X_d_tv, X_d_tv)
    Q_inv[:nb_var-1, -1] = X_d_tv
    Q_inv[-1, :nb_var-1] = X_d_tv
    Q_inv[-1, -1] = 1
    Q[tv_slice, tv_slice] = np.linalg.inv(Q_inv)

# Dynamical System settings (discrete)
# =====================================

A1d = np.zeros((param.nbDeriv,param.nbDeriv))
B1d = np.zeros((param.nbDeriv,1))

for i in range(param.nbDeriv):
    A1d += np.diag( np.ones(param.nbDeriv-i), i ) * param.dt**i * 1/factorial(i)
    B1d[param.nbDeriv-i-1] = param.dt**(i+1) * 1/factorial(i+1)

A = np.eye(nb_var)
A[:nb_var-1, :nb_var-1] = np.kron(A1d, np.identity(param.nbVarPos))
B = np.zeros((nb_var, param.nbVarPos))
B[:nb_var-1] = np.kron(B1d, np.identity(param.nbVarPos))

# Build Sx and Su transfer matrices
Su = np.zeros((nb_var*param.nbData, param.nbVarPos * (param.nbData-1))) 
Sx = np.kron(np.ones((param.nbData,1)), np.eye(nb_var,nb_var))

M = B
for i in range(1,param.nbData):
    Sx[i*nb_var:param.nbData*nb_var,:] = np.dot(Sx[i*nb_var:param.nbData*nb_var,:], A)
    Su[nb_var*i:nb_var*i+M.shape[0],0:M.shape[1]] = M
    M = np.hstack((np.dot(A,M),B)) # [0,nb_state_var-1]

# Build recursive least squares solution for the feedback gains, using u_k = - F_k @ x0 = - K_k @ x_k
# =====================================
F = np.linalg.inv(Su.T @ Q @ Su + R) @ Su.T @ Q @ Sx
K = np.zeros((param.nbData-1, param.nbVarPos, nb_var))
P = np.eye(nb_var)
K[0] = F[:param.nbVarPos]
for k in range(1, param.nbData-1):
  k_slice = slice(k*param.nbVarPos, (k+1)*param.nbVarPos)
  P = P @ np.linalg.inv(A - B @ K[k-1])
  K[k] = F[k_slice] @ P

# Multiple reproductions from different initial positions with perturbation
# =====================================
num_repro = 10

x = np.zeros((nb_var, num_repro))
x[:param.nbVarPos, :] = np.random.normal(0, 1e-1, (param.nbVarPos, num_repro))
x[-1,:] = np.ones(num_repro)

X_batch = []
X_batch.append(x[:-1,:].copy())

for k in range(param.nbData-1):
  
  u = - K[k] @ x
  x = A @ x + B @ (u + np.random.normal(0, 1e+1, (param.nbVarPos, num_repro)))
  X_batch.append(x[:-1,:].copy())
  
X_traj = np.array(X_batch)

# Plotting
# =========
plt.figure()
plt.title("2D Trajectory")
plt.scatter(X_traj[0,0], X_traj[0,1], c='black', s=100)
plt.plot(X_traj[:,0], X_traj[:,1], c='black')
for p in via_points:
  plt.scatter(p[0], p[1], c='red', s=100)  
plt.axis("off")
plt.gca().set_aspect('equal', adjustable='box')

plt.show()
